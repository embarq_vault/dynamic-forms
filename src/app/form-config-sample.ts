export const formConfigSample = {
  "name": "article",
  "description": "",
  "displayField": "title",
  "fields": [
    {
      "id": "title",
      "name": "title",
      "type": "Text",
      "placeHolder": "this is my title",
      "localized": false,
      "required": false,
      "validations": [
        {
          "required": true
        },
        {
          "maxLength": 100
        }
      ],
      "disabled": false,
      "omitted": false
    },
    {
      "id": "description",
      "name": "description",
      "type": "RichText",
      "localized": false,
      "required": false,
      "validations": [
        {
          "nodes": {
            
          }
        },
        {
          "enabledMarks": [
            "bold",
            "italic"
          ],
          "message": "Only bold and italic marks are allowed"
        },
        {
          "enabledNodeTypes": [
            "heading-2",
            "heading-3",
            "heading-5",
            "heading-6",
            "ordered-list",
            "unordered-list",
            "hr",
            "blockquote",
            "embedded-entry-block",
            "embedded-asset-block",
            "entry-hyperlink",
            "asset-hyperlink",
            "heading-4"
          ],
          "message": "Only heading 2, heading 3, heading 5, heading 6, ordered list, unordered list, horizontal rule, quote, block entry, asset, link to entry, link to asset, and heading 4 nodes are allowed"
        }
      ],
      "disabled": false,
      "omitted": false
    },
    {
      "id": "myLocation",
      "name": "my location",
      "type": "Location",
      "localized": false,
      "required": false,
      "validations": [
        
      ],
      "disabled": false,
      "omitted": false
    },
    {
      "id": "number",
      "name": "number",
      "type": "Integer",
      "localized": false,
      "required": false,
      "validations": [
        
      ],
      "disabled": false,
      "omitted": false
    },
    {
      "id": "boolean",
      "name": "boolean",
      "type": "Boolean",
      "localized": false,
      "required": false,
      "validations": [
        
      ],
      "disabled": false,
      "omitted": false
    },
    {
      "id": "json",
      "name": "json",
      "type": "Object",
      "localized": false,
      "required": false,
      "validations": [
        
      ],
      "disabled": false,
      "omitted": false,
      "fields": [
        {
          "id": "title",
          "name": "title",
          "type": "Text",
          "localized": false,
          "required": false,
          "validations": [
            {
              "required": true
            },
            {
              "maxLength": 100
            }
          ],
          "disabled": false,
          "omitted": false
        },
        {
          "id": "description",
          "name": "description",
          "type": "RichText",
          "localized": false,
          "required": false,
          "validations": [
            {
              "nodes": {
                
              }
            },
            {
              "enabledMarks": [
                "bold",
                "italic"
              ],
              "message": "Only bold and italic marks are allowed"
            },
            {
              "enabledNodeTypes": [
                "heading-2",
                "heading-3",
                "heading-5",
                "heading-6",
                "ordered-list",
                "unordered-list",
                "hr",
                "blockquote",
                "embedded-entry-block",
                "embedded-asset-block",
                "entry-hyperlink",
                "asset-hyperlink",
                "heading-4"
              ],
              "message": "Only heading 2, heading 3, heading 5, heading 6, ordered list, unordered list, horizontal rule, quote, block entry, asset, link to entry, link to asset, and heading 4 nodes are allowed"
            }
          ],
          "disabled": false,
          "omitted": false
        }
      ]
    },
    {
      "id": "testSchedule",
      "name": "test schedule",
      "type": "Date",
      "localized": false,
      "required": false,
      "validations": [
        
      ],
      "disabled": false,
      "omitted": false
    },
    {
      "id": "select",
      "name": "drop down ",
      "type": "Select",
      "comments": "appearance can be <dropdown>, <checkbox (only if multi allowed)>, <radio (only if multi not allowed)>",
      "appearance": "dropdown",
      "allowMulti": true,
      "options": [
        {
          "value": 1,
          "text": "value 1"
        },
        {
          "value": 2,
          "text": "value 2"
        },
        {
          "value": 3,
          "text": "value 3"
        }
      ],
      "localized": false,
      "required": false,
      "validations": [
        
      ],
      "disabled": false,
      "omitted": false
    },
    {
      "id": "checkboxes",
      "name": "Checkbox list",
      "type": "Select",
      "comments": "appearance can be <dropdown>, <checkbox (only if multi allowed)>, <radio (only if multi not allowed)>",
      "allowMulti": true,
      "options": [
        {
          "value": 1,
          "text": "value 1"
        },
        {
          "value": 2,
          "text": "value 2"
        },
        {
          "value": 3,
          "text": "value 3"
        }
      ],
      "localized": false,
      "required": false,
      "validations": [
        
      ],
      "disabled": false,
      "omitted": false
    },
    {
      "id": "radio-buttons",
      "name": "Radio-list",
      "type": "Select",
      "comments": "appearance can be <dropdown>, <checkbox (only if multi allowed)>, <radio (only if multi not allowed)>",
      "allowMulti": false,
      "options": [
        {
          "value": 1,
          "text": "value 1"
        },
        {
          "value": 2,
          "text": "value 2"
        },
        {
          "value": 3,
          "text": "value 3"
        }
      ],
      "localized": false,
      "required": false,
      "validations": [
        
      ],
      "disabled": false,
      "omitted": false
    }
  ],
  "sys": {
    "space": {
      "sys": {
        "type": "Link",
        "linkType": "Space",
        "id": "7buc9oojvm74"
      }
    },
    "id": "article",
    "type": "ContentType",
    "createdAt": "2019-08-04T02:25:30.743Z",
    "updatedAt": "2019-10-15T20:44:57.038Z",
    "environment": {
      "sys": {
        "id": "master",
        "type": "Link",
        "linkType": "Environment"
      }
    },
    "publishedVersion": 9,
    "publishedAt": "2019-10-15T20:44:57.038Z",
    "firstPublishedAt": "2019-08-04T02:25:31.088Z",
    "createdBy": {
      "sys": {
        "type": "Link",
        "linkType": "User",
        "id": "76N6f4MQVaUgSvCghicNG8"
      }
    },
    "updatedBy": {
      "sys": {
        "type": "Link",
        "linkType": "User",
        "id": "76N6f4MQVaUgSvCghicNG8"
      }
    },
    "publishedCounter": 5,
    "version": 10,
    "publishedBy": {
      "sys": {
        "type": "Link",
        "linkType": "User",
        "id": "76N6f4MQVaUgSvCghicNG8"
      }
    }
  }
};