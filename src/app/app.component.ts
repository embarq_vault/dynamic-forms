import { Component, OnInit } from '@angular/core';
import { formConfigSample } from './form-config-sample';
import { DynamicFormConfig } from './dynamic-form/dynamic-form.model';

@Component({
  selector: 'app-root',
  template: `
    <app-dynamic-form
      [config]="dynamicFormConfig"
      (init)="handleDynamicFormInit($event)">
    </app-dynamic-form>
  `,
  styles: []
})
export class AppComponent implements OnInit {
  dynamicFormConfig: DynamicFormConfig;

  constructor() {}

  ngOnInit() {
    this.dynamicFormConfig = formConfigSample;
  }

  handleDynamicFormInit(form) {
    form.valueChanges.subscribe(
      () => console.log('form:changes', form.value)
    );
  }
}
