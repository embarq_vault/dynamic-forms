export interface DynamicFormConfig {
  name: string;
  description: string;
  displayField: string;
  fields: DynamicFormField[];
  sys: DynamicFormSys;
}

export interface DynamicFormSys {
  space: DynamicFormSpace;
  id: string;
  type: string;
  createdAt: string;
  updatedAt: string;
  environment: DynamicFormSpace;
  publishedVersion: number;
  publishedAt: string;
  firstPublishedAt: string;
  createdBy: DynamicFormSpace;
  updatedBy: DynamicFormSpace;
  publishedCounter: number;
  version: number;
  publishedBy: DynamicFormSpace;
}

export interface DynamicFormSpace {
  sys: {
    type: string;
    linkType: string;
    id: string;
  };
}

export type DynamicFormFieldType
  = 'Text'
  | 'RichText'
  | 'Location'
  | 'Integer'
  | 'Boolean'
  | 'Object'
  | 'Date'
  | 'Select';

export interface DynamicFormField {
  id: string;
  name: string;
  type: string;
  placeHolder?: string;
  localized: boolean;
  required: boolean;
  validations: DynamicFormValidation[];
  disabled: boolean;
  omitted: boolean;
  fields?: DynamicFormField[];
  comments?: string;
  appearance?: string;
  allowMulti?: boolean;
  options?: DynamicFormOption[];
}

export interface DynamicFormOption {
  value: number;
  text: string;
}

export interface DynamicFormValidation {
  required?: boolean;
  maxLength?: number;
  nodes?: any;
  enabledMarks?: string[];
  message?: string;
  enabledNodeTypes?: string[];
}