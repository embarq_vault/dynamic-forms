import { Component, OnInit, Input, ViewChildren, QueryList, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { DynamicFormField } from './dynamic-form.model';
import { FormGroup } from '@angular/forms';
import { AbstractDynamicFormField } from './field/field-base';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit, AfterViewInit, AbstractDynamicFormField {
  @Input() config: DynamicFormField;
  @Output() init: EventEmitter<FormGroup>;

  @ViewChildren('dynamicFormField')
  public fields: QueryList<AbstractDynamicFormField>;

  public control: FormGroup;

  constructor() {
    this.init = new EventEmitter();
  }

  ngOnInit() {
    this.control = new FormGroup({});
  }

  ngAfterViewInit() {
    this.fields.forEach(field => {
      this.control.addControl(field.config.id, field.getControl());
    });

    this.init.emit(this.control);
  }

  getControl() {
    return this.control;
  }

}
