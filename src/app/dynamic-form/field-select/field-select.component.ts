import { Component, OnInit, Input } from '@angular/core';
import { AbstractDynamicFormField } from '../field/field-base';
import { DynamicFormField } from '../dynamic-form.model';
import { FormControl } from '@angular/forms';
import { DynamicFormFieldConfigurationService } from '../dynamic-form-field-configuration.service';

@Component({
  selector: 'app-field-select',
  templateUrl: './field-select.component.html',
  styleUrls: ['./field-select.component.scss']
})
export class FieldSelectComponent implements OnInit, AbstractDynamicFormField {
  @Input() config: DynamicFormField;
  control: FormControl;

  constructor(
    private validationMapperService: DynamicFormFieldConfigurationService
  ) { }

  ngOnInit() {
    const validators = this.config.validations.map(
      entry => this.validationMapperService.getValidationMethod(entry)
    );
    this.control = new FormControl(null, validators);
  }

  getControl() {
    return this.control;
  }
}
