import { Component, OnInit, Input } from '@angular/core';
import { AbstractDynamicFormField } from '../field/field-base';
import { DynamicFormField } from '../dynamic-form.model';
import { FormControl } from '@angular/forms';
import { DynamicFormFieldConfigurationService } from '../dynamic-form-field-configuration.service';

@Component({
  selector: 'app-field-date',
  templateUrl: './field-date.component.html',
  styleUrls: ['./field-date.component.scss']
})
export class FieldDateComponent implements OnInit, AbstractDynamicFormField {
  @Input() config: DynamicFormField;
  control: FormControl;

  constructor(
    private validationMapperService: DynamicFormFieldConfigurationService
  ) { }

  ngOnInit() {
    const validators = this.config.validations.map(
      entry => this.validationMapperService.getValidationMethod(entry)
    );
    this.control = new FormControl(null, validators);
  }

  getControl() {
    return this.control;
  }
}
