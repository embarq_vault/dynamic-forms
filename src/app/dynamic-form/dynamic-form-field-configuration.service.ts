import { DynamicFormValidation, DynamicFormField } from './dynamic-form.model';
import { Validators } from '@angular/forms';

export class DynamicFormFieldConfigurationService {
  private fieldTypeMap = {
    Text: 'text',
    RichText: 'RichText',
    Location: 'Location',
    Integer: 'number',
    Boolean: 'radio',
    Object: 'Object',
    Date: 'date',
    Select: 'select',
  }

  mapFieldType(type: DynamicFormField['type']) {
    return this.fieldTypeMap[type];
  }

  getValidationMethod(validationConfig: DynamicFormValidation) {
    const validators = Object
      .keys(validationConfig)
      .map(validationKey => {
        switch (validationKey) {
          case 'required': return Validators.required
          case 'maxLength': return Validators.maxLength(validationConfig.maxLength)
        }
      })
      .filter(validator => validator != null);

    return Validators.compose(validators);
  } 
}