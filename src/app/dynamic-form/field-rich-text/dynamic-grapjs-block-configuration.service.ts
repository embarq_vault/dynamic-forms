import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DynamicGrapjsBlockConfigurationService {

  getGrapjsBlockConfigurationMethod(config) {
    const block = config.map(item => {
      console.log(item);
      switch (item) {
        case 'heading-2': return {
          id: 'heading-2',
          label: `<i class="fa fa-font" aria-hidden="true"></i><div class="my-label-block">Heading-2</div>`,
          attributes: { class: 'gjs-block-heading-2' },
          content: `<h2>Default text</h2>`,
        }
        case 'heading-3': return {
          id: 'heading-3',
          label: `<i class="fa fa-font" aria-hidden="true"></i><div class="my-label-block">Heading-3</div>`,
          attributes: { class: 'gjs-block-heading-3' },
          content: '<h3>Default text</h3>',
        }
        case 'heading-4': return {
          id: 'heading-4',
          label: `<i class="fa fa-font" aria-hidden="true"></i><div class="my-label-block">Heading-4</div>`,
          attributes: { class: 'gjs-block-heading-4' },
          content: '<h4>Default text</h4>',
        }
        case 'heading-5': return {
          id: 'heading-5',
          label: `<i class="fa fa-font" aria-hidden="true"></i><div class="my-label-block">Heading-5</div>`,
          attributes: { class: 'gjs-block-heading-5' },
          content: '<h5>Default text</h5>',
        }
        case 'heading-6': return {
          id: 'heading-6',
          label: `<i class="fa fa-font" aria-hidden="true"></i><div class="my-label-block">Heading-6</div>`,
          attributes: { class: 'gjs-block-heading-6' },
          content: '<h6>Default text</h6>',
        }
        case 'hr': return {
          id: 'hr',
          label: `<i class="fa fa-horizontal-rule"></i><div class="my-label-block">Divider</div>`,
          attributes: { class: 'gjs-block-divider' },
          content: '<hr>',
        }
        case 'blockquote': return {
          id: 'blockquote',
          label: `<i class="fa fa-file-text-o" aria-hidden="true"></i><div class="my-label-block">Blockquote</div>`,
          attributes: { class: 'gjs-block-blockquote' },
          content: `<blockquote><p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p></blockquote>`,
        }
        case 'image': return {
          id: 'image',
          label: `<i class="fa fa-picture-o" aria-hidden="true"></i><div class="my-label-block">Image</div>`,
          // Select the component once dropped in canavas
          select: true,
          attributes: { class: 'gjs-block-image' },
          // You can pass components as a JSON instead of a simple HTML string,
          // in this case we also use a defined component type `image`
          content: { type: 'image' },
          // This triggers `active` on dropped components
          activate: true,

        }
      }
    }).filter(block => block != null);
    console.log(block)
    return block;
  }
}
