import { Component, OnInit, Input } from '@angular/core';
import { AbstractDynamicFormField } from '../field/field-base';
import { DynamicFormField } from '../dynamic-form.model';
import { FormControl } from '@angular/forms';
import { DynamicFormFieldConfigurationService } from '../dynamic-form-field-configuration.service';
import { DynamicGrapjsBlockConfigurationService } from './dynamic-grapjs-block-configuration.service';
import grapesjs from 'grapesjs';
@Component({
  selector: 'app-field-rich-text',
  templateUrl: './field-rich-text.component.html',
  styleUrls: ['./field-rich-text.component.scss']
})
export class FieldRichTextComponent implements OnInit, AbstractDynamicFormField {
  @Input() config: DynamicFormField;
  control: FormControl;
  configBlockManager;
  configRichTextEditor;
  constructor(
    private grapjsBlockService: DynamicGrapjsBlockConfigurationService
  ) { }

  ngOnInit() {
    const blockManagerConfig = this.config.validations.filter(item => item.hasOwnProperty("enabledNodeTypes"));
    if (blockManagerConfig.length > 0) {
      this.configBlockManager = this.grapjsBlockService.getGrapjsBlockConfigurationMethod(blockManagerConfig[0].enabledNodeTypes);
    } else {
      this.configBlockManager = null;
    }
    this.configRichTextEditor = this.config.validations.filter(item => item.hasOwnProperty("enabledMarks"));

    this.initDescription();
  }

  getControl() {
    return this.control;
  }

  public initDescription() {
    const editor = grapesjs.init({
      container: '#gjs',
      adjustToolbar: 1,
      stylePrefix: 'gjs-',
      fromElement: true,
      storageManager: false,
      // Avoid any default panel
      panels: {},
      richTextEditor: {
        actions: this.configRichTextEditor[0].enabledMarks,
      },
      height: '300px',
      width: 'auto',
      blockManager: {

      },
    });
    if (this.configBlockManager) {
      this.configBlockManager.map(block => {
        editor.BlockManager.add(block.id, block);
      });
    }
    editor.RichTextEditor.add('orderedList',
      {
        icon: `<i class="fa fa-list-ol" aria-hidden="true"></i>`,
        attributes: { title: 'Ordered List' },
        result: rte => rte.exec('insertOrderedList')
      });
    editor.RichTextEditor.add('unorderedList',
      {
        icon: `<i class="fa fa-list-ul" aria-hidden="true"></i>`,
        attributes: { title: 'Unordered List' },
        result: rte => rte.exec('insertUnorderedList')
      });
  }
}
