import { DynamicFormField } from '../dynamic-form.model';
import { AbstractControl } from '@angular/forms';

export interface AbstractDynamicFormField {
  config: DynamicFormField;
  control: AbstractControl;

  getControl(): AbstractControl;
}