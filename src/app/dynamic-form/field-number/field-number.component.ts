import { Component, OnInit, Input } from '@angular/core';
import { DynamicFormField } from '../dynamic-form.model';
import { FormControl } from '@angular/forms';
import { DynamicFormFieldConfigurationService } from '../dynamic-form-field-configuration.service';
import { AbstractDynamicFormField } from '../field/field-base';

@Component({
  selector: 'app-field-number',
  templateUrl: './field-number.component.html',
  styleUrls: ['./field-number.component.scss']
})
export class FieldNumberComponent implements OnInit, AbstractDynamicFormField {
  @Input() config: DynamicFormField;
  control: FormControl;

  constructor(
    private validationMapperService: DynamicFormFieldConfigurationService
  ) { }

  ngOnInit() {
    const validators = this.config.validations.map(
      entry => this.validationMapperService.getValidationMethod(entry)
    );
    this.control = new FormControl(null, validators);
  }

  getControl() {
    return this.control;
  }

}
