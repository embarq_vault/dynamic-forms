import { Component, OnInit, Input } from '@angular/core';
import { AbstractDynamicFormField } from '../field/field-base';
import { DynamicFormField } from '../dynamic-form.model';
import { FormControl, FormGroup } from '@angular/forms';
import { DynamicFormFieldConfigurationService } from '../dynamic-form-field-configuration.service';

@Component({
  selector: 'app-field-checkbox',
  templateUrl: './field-checkbox.component.html',
  styleUrls: ['./field-checkbox.component.scss']
})
export class FieldCheckboxComponent implements OnInit, AbstractDynamicFormField {
  @Input() config: DynamicFormField;
  control: FormGroup;

  constructor(
    private validationMapperService: DynamicFormFieldConfigurationService
  ) { }

  ngOnInit() {
    // TODO: implement "at least one option checked is required" validator for the group
    const validators = this.config.validations.map(
      entry => this.validationMapperService.getValidationMethod(entry)
    );
    const controls = this.config.options.reduce((controls, option) => {
      controls[option.text] = new FormControl(false);
      return controls;
    }, {});
    this.control = new FormGroup(controls, validators);
  }

  getControl() {
    return this.control;
  }

}
