import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from '@angular/forms';
import { DynamicFormComponent } from './dynamic-form.component';
import { FieldComponent } from './field/field.component';
import { DynamicFormFieldConfigurationService } from './dynamic-form-field-configuration.service';
import { CommonModule } from '@angular/common';
import {
  MatInputModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatSelectModule,
  MatButtonModule,
  MatCheckboxModule,
  MatRadioModule,
  MatCardModule
} from '@angular/material';
import { FieldRichTextComponent } from './field-rich-text/field-rich-text.component';
import { FieldNumberComponent } from './field-number/field-number.component';
import { FieldDateComponent } from './field-date/field-date.component';
import { FieldSelectComponent } from './field-select/field-select.component';
import { FieldCheckboxComponent } from './field-checkbox/field-checkbox.component';
import { FieldRadioComponent } from './field-radio/field-radio.component';
import { DynamicGrapjsBlockConfigurationService } from './field-rich-text/dynamic-grapjs-block-configuration.service';

const declarations = [
  DynamicFormComponent,
  FieldComponent,
  FieldRichTextComponent,
  FieldNumberComponent,
  FieldDateComponent,
  FieldSelectComponent,
  FieldCheckboxComponent,
  FieldRadioComponent
];

const materialImports = [
  MatInputModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatSelectModule,
  MatButtonModule,
  MatCheckboxModule,
  MatRadioModule,
  MatCardModule
]

@NgModule({
  declarations: [ ...declarations  ],
  exports: [ DynamicFormComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ...materialImports
  ],
  providers: [ DynamicFormFieldConfigurationService , DynamicGrapjsBlockConfigurationService ]
})
export class DynamicFormModule {}